<?php namespace Hampel\Validate\LaravelAuth;
/**
 * 
 */


class Validator extends \Illuminate\Validation\Validator
{
	public function validateAuth($attribute, $value, $parameters)
	{
		if (count($parameters) < 2) return false;

		$credentials = array();

		$count = 0;
		$credential_field = "";
		foreach($parameters as $parameter)
		{
			$count++;

			if ($count % 2 == 0)
			{
				$credentials[$credential_field] = $parameter;
			}
			else
			{
				$credential_field = $parameter;
			}
		}

		$credentials['password'] = $value;

		return $this->container['auth']->validate($credentials);
	}
}
