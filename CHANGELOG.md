CHANGELOG
=========

1.3.1 (2015-05-22)
------------------

* removed redundant closing php tag

1.3.0 (2014-07-08)
------------------

* new functionality for auth validation function - can supply multiple credentials
* refactored unit tests; added new unit tests for new code

1.2.4 (2014-06-06)
------------------

* removed dev dependency on phpunit
* less restrictive versioning for framework dependencies

1.2.3 (2013-12-17)
------------------

* bug - wrong package name when calling translator

1.2.2 (2013-12-17)
------------------

* made service provdier a bit more modular
* changed service provider test to actually call the boot method (with partial mock of the service provider)

1.2.1 (2013-12-15)
------------------

* removed orchestral/testbench from require-dev added missing required Illuminate packages
* simplified service provider code
* refactored Validator to avoid using Facades, use container from base Validator class instead
* rewrote unit tests to not use Orchestra Testbench

1.2.0 (2013-12-13)
------------------

* updated framework requirement to 4.1.*

1.1.3 (2013-10-21)
------------------

* implemented passing of translation array to validator via Factory::extend method
* minimum version of illuminate/validation set to 4.0.9 to use additional parameter on Factory::extend

1.1.2 (2013-10-15)
------------------

* bound an instance of our custom Validator class to container to allow us to pass dependencies

1.1.1 (2013-10-14)
------------------

* service provider update, more unit tests

1.1.0 (2013-10-13)
------------------

* rewrote service provider to use extend() rather than resolve() to add additional validation functions - turns out that you can only use
 a class which extends the validation class once - so if you have two packages or service providers which both extend the validation class,
 only one will be used. Adding extensions to the base class one-by-one gets around this limitation.

1.0.0 (2013-09-27)
------------------

* initial release, split from hampel/validate-laravel package
