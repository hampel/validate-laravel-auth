<?php namespace Hampel\Validate\LaravelAuth;
/**
 * 
 */

use Mockery;
use Hampel\Validate\LaravelAuth\Validator as ValidatorClass;

class ValidatorTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		parent::setUp();

		$this->lang = Mockery::mock('Illuminate\Translation\Translator');
		$this->db = Mockery::mock('Illuminate\Validation\PresenceVerifierInterface');
		$this->auth = Mockery::mock('Illuminate\Auth\Guard');
		$this->container = Mockery::mock('Illuminate\Container\Container');
	}

	public function testValidateAuthPasses()
	{
		$this->db->shouldReceive('getCount')->never();
		$this->auth->shouldReceive('validate')->once()->with(array('username' => 'foo', 'password' => 'bar'))->andReturn(true);
		$this->container->shouldReceive('offsetGet')->once()->with('auth')->andReturn($this->auth);

		$v = new ValidatorClass($this->lang, array('password' => 'bar'), array('password' => 'auth:username,foo'));
		$v->setContainer($this->container);
		$v->setPresenceVerifier($this->db);
		@$this->assertTrue($v->passes());
	}

	public function testValidateAuthFails()
	{
		$this->db->shouldReceive('getCount')->never();
		$this->auth->shouldReceive('validate')->once()->with(array('username' => 'foo', 'password' => 'bar'))->andReturn(false);
		$this->container->shouldReceive('offsetGet')->once()->with('auth')->andReturn($this->auth);
		$this->lang->shouldReceive('trans')->once()->with('validation.custom.password.auth')->andReturn('Invalid :attribute');
		$this->lang->shouldReceive('trans')->once()->with('validation.attributes.password')->andReturn('password');

		$v = new ValidatorClass($this->lang, array('password' => 'bar'), array('password' => 'auth:username,foo'));
		$v->setContainer($this->container);
		$v->setPresenceVerifier($this->db);
		@$this->assertTrue($v->fails());
	}

	public function testValidateAuthPassesDifferentUsername()
	{
		$this->db->shouldReceive('getCount')->never();

		// test validation passes
		$this->auth->shouldReceive('validate')->once()->with(array('user_login' => 'foo', 'password' => 'bar'))->andReturn(true);
		$this->container->shouldReceive('offsetGet')->once()->with('auth')->andReturn($this->auth);

		$v = new ValidatorClass($this->lang, array('password' => 'bar'), array('password' => 'auth:user_login,foo'));
		$v->setContainer($this->container);
		$v->setPresenceVerifier($this->db);
		$this->assertTrue($v->passes());
	}

	public function testValidateAuthFailsDifferentUsername()
	{
		$this->db->shouldReceive('getCount')->never();

		// test validation fails
		$this->auth->shouldReceive('validate')->once()->with(array('user_login' => 'foo', 'password' => 'bar'))->andReturn(false);
		$this->container->shouldReceive('offsetGet')->once()->with('auth')->andReturn($this->auth);

		$this->lang->shouldReceive('trans')->once()->with('validation.custom.password.auth')->andReturn('Invalid :attribute');
		$this->lang->shouldReceive('trans')->once()->with('validation.attributes.password')->andReturn('password');

		$v = new ValidatorClass($this->lang, array('password' => 'bar'), array('password' => 'auth:user_login,foo'));
		$v->setContainer($this->container);
		$v->setPresenceVerifier($this->db);
		$this->assertTrue($v->fails());
	}

	public function testValidateAuthPassesMultiple()
	{
		$this->db->shouldReceive('getCount')->never();

		// test validation with multiple credentials
		$this->auth->shouldReceive('validate')->once()->with(array('user_login' => 'foo', 'password' => 'bar', 'user_email' => 'bam@example.com'))->andReturn(true);
		$this->container->shouldReceive('offsetGet')->once()->with('auth')->andReturn($this->auth);

		$v = new ValidatorClass($this->lang, array('password' => 'bar'), array('password' => 'auth:user_login,foo,user_email,bam@example.com'));
		$v->setContainer($this->container);
		$v->setPresenceVerifier($this->db);
		$this->assertTrue($v->passes());
	}

	public function testValidateAuthPassesIncomplete()
	{
		$this->db->shouldReceive('getCount')->never();

		// test validation with incompletely supplied credentials
		$this->auth->shouldReceive('validate')->once()->with(array('user_login' => 'foo', 'password' => 'bar'))->andReturn(true);
		$this->container->shouldReceive('offsetGet')->once()->with('auth')->andReturn($this->auth);

		$v = new ValidatorClass($this->lang, array('password' => 'bar'), array('password' => 'auth:user_login,foo,user_email'));
		$v->setContainer($this->container);
		$v->setPresenceVerifier($this->db);
		$this->assertTrue($v->passes());
	}

	public function tearDown() {
		Mockery::close();
	}
}
