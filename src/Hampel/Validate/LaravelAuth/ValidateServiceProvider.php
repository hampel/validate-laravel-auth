<?php namespace Hampel\Validate\LaravelAuth;

use Illuminate\Support\ServiceProvider;

class ValidateServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	protected $rules = array(
		'auth'
	);

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('hampel/validate-laravel-auth', 'validate-laravel-auth', __DIR__ . '/../../..');

		$app = $this->app;

		$this->app->bind('Hampel\Validate\LaravelAuth\Validator', function($app)
		{
			$validator = new Validator($app['translator'], array(), array(), $app['translator']->get('validate-laravel-auth::validation'));

			if (isset($app['validation.presence']))
			{
				$validator->setPresenceVerifier($app['validation.presence']);
			}

			$validator->setContainer($app);

			return $validator;
		});

		$this->addNewRules();
	}

	protected function addNewRules()
	{
		foreach ($this->getRules() as $rule)
		{
			$this->extendValidator($rule);
		}
	}

	protected function extendValidator($rule)
	{
		$method = 'validate' . studly_case($rule);
		$translation = $this->app['translator']->get('validate-laravel-auth::validation');

		$this->app['validator']->extend($rule, 'Hampel\Validate\LaravelAuth\Validator@' . $method, $translation[$rule]);
	}

	public function getRules()
	{
		return $this->rules;
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}