Hampel Laravel Auth Validator
=============================

Custom Auth Validators for Laravel 4

By [Simon Hampel](http://hampelgroup.com/).

Notes
-----

I have decided not to update this package to work with Laravel 5 - this functionality is provided by the built-in 
Auth Controller and avoids the complexity of trying to authenticate users inside a validation rule.

For more information, have a look at the Laravel 5 trait [AuthenticatesAndRegistersUsers](https://github.com/laravel/framework/blob/5.0/src/Illuminate/Foundation/Auth/AuthenticatesAndRegistersUsers.php#L71)

    :::php
    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
    	$this->validate($request, [
    		'email' => 'required', 'password' => 'required',
    	]);
    	$credentials = $request->only('email', 'password');
    	if ($this->auth->attempt($credentials, $request->has('remember')))
    	{
    		return redirect()->intended($this->redirectPath());
    	}
    	return redirect($this->loginPath())
    				->withInput($request->only('email', 'remember'))
    				->withErrors([
    					'email' => 'These credentials do not match our records.',
    				]);
    }

This effectively achieves the same result with no coding required by us!

However, I do recognise that there are other uses for the validation library, so if there is a use-case for including
auth functionality in the validation library which I haven't considered, please let me know and I'll revisit
releasing a L5 version of this package.

Installation
------------

The recommended way of installing Hampel Laravel Auth Validator is through [Composer](http://getcomposer.org):

Require the package via Composer in your `composer.json`

	:::json
    {
        "require": {
            "hampel/validate-laravel-auth": "1.3.*"
        }
    }

Run Composer to update the new requirement.

	:::bash
    $ composer update

The package is built to work with the Laravel 4 Framework.

Open your Laravel config file `config/app.php` and add the service provider in the `$providers` array:

	:::php
    "providers" => array(

        ...

        "Hampel\Validate\LaravelAuth\ValidateServiceProvider"

    ),

Usage
-----

This package adds an additional validator for Laravel 4 - refer to
[Laravel Documentation - Validation](http://laravel.com/docs/validation) for general usage instructions.

__auth:*credential_column1*,*credential_value1*,*credential_column2*,*credential_value2*__,...__

The field under validation must be a password, which is combined with the supplied credential column names and values
and then passed to Auth::validate to check whether they are a valid password/credential combination.

At least two parameters to the auth rule are required (if less than two are provided, the rule will always fail).

1. __credential_column1__: name of the first credential column field, eg: 'username' ('user_login' in WordPress), or
'email', ('user_email' in WordPress).
2. __credential_value1__: value of the first credential to locate in the first credential column
3. __credential_column2__: name of the second credential column
4. __credential_value2__: value of the second credential
5. ...

In this way, multiple credentials can be specified to validate a user against (eg locate a user using both username and
email).

Example
-------

	:::php
	$username_field = "user_login";

	$username = Input::get('username');
	$password = Input::get('password');

	$user_table = Config::get('auth.table');

	$userdata = array(
		'username' => $username,
		'password' => $password
	);

	// Declare the rules for the form validation.
	$rules = array(
		'username' => array('required', "exists:{$user_table},{$username_field}"),
		'password' => array('required', "auth:{$username_field},{$username}")
	);

	// Validate the inputs.
	$validator = Validator::make($userdata, $rules);

	// Check if the form validates with success.
	if ($validator->passes())
	{
		dd("passes");
	}
	else
	{
		dd("fails");
	}
